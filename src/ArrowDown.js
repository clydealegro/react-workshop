import React, { Component } from 'react';

export default class ArrowDown extends Component {
    render() {
        const { iconfill } = this.props;

        return (
            <svg className="arrowdown-icon" viewBox="0 0 837.50001 557.81245">
                <title id="arrow-down">ArrowDown Icon</title>
                <path fill={iconfill} d="M209.06 237.186C94.08 122.2 0 27.27 0 26.226 0 23.78 23.825 0 26.274 0c1.02 0 89.603 87.75 196.85 195 107.25 107.25 195.562 195 196.25 195 .69 0 89.003-87.75 196.25-195C722.875 87.75 811.197 0 811.898 0 813.91 0 837.5 24.21 837.5 26.274c0 1.622-417.135 419.976-418.753 419.976-.344 0-94.702-94.08-209.686-209.064z"/>
            </svg>
        );
    }
}