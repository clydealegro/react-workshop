import React, { Component } from 'react';

export default class ArrowUp extends Component {
    render() {
        const { iconfill } = this.props;

        return (
            <svg className="arrowup-icon" viewBox="0 0 837.50002 558.5901625">
                <title id="arrow-up">ArrowUp Icon</title>
                <path fill={iconfill} d="M12.164 434.662C5.474 427.947 0 421.618 0 420.598c0-1.02 94.22-96.07 209.375-211.226L418.75 0l209.375 209.372C743.28 324.527 837.5 419.6 837.5 420.644c0 2.06-23.633 26.228-25.65 26.228-.675 0-88.977-87.75-196.225-195-107.248-107.25-195.56-195-196.25-195-.69 0-89.002 87.75-196.25 195s-195.852 195-196.897 195c-1.045 0-7.374-5.494-14.064-12.21z"/>
            </svg>
        );
    }
}