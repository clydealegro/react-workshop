import React, { Component } from 'react';
import { style } from 'typestyle';
import AccordionItem from './AccordionItem';

const className = style({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    $nest: {
        'ul': {
            listStyle: 'none',
            margin: '0',
            padding: '0',
            width: '500px',
        },
    }
});

class App extends Component {
    render() {
        return (
            <div className={className}>
                <ul>
                    <AccordionItem label="React" />
                    <AccordionItem label="AngularJS" />
                    <AccordionItem label="EmberJS" />
                    <AccordionItem label="Vue" />
                    <AccordionItem label="Express" />
                </ul>
            </div>
        );
    }
}

export default App;
