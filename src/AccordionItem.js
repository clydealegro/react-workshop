import React, { Component } from 'react';
import { style } from 'typestyle';
import classnames from 'classnames';
import ArrowUp from './ArrowUp';
import ArrowDown from './ArrowDown';

const className = style({
	padding: '20px',
	borderBottom: '1px solid #ddd',
    $nest: {
    	'&.selected': {
			backgroundColor: '#f2f2f2',
    	},
    	'&:first-child': {
    		borderTop: '1px solid #ddd',
    	},
    	'&:hover': {
			cursor: 'pointer',
    	},
        'strong': {
            color: '#444',
            display: 'flex',
            fontWeight: 'bold',
            justifyContent: 'space-between',
            textDecoration: 'none',
        },
        'strong svg': {
        	width: '12px',
        	height: '12px',
        	marginTop: '5px',
        },
        '&.selected strong': {
        	marginBottom: '10px',
        },
        '.desc': {
        	display: 'none',
        },
        '&.selected .desc': {
        	display: 'block',
        }
    }
});


class AccordionItem extends Component {
	constructor(props) {
        super(props);
    }    

    render() {
    	return(
			<li className={className}>
			    <strong>
			    	{this.props.label}
			    </strong>
			    <div className="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci facere aperiam amet doloribus tempore. Unde excepturi ipsum dolorum. Iusto sapiente officia saepe. Ipsa laudantium, quos laborum voluptates! Dolorum, rem, doloribus.</div>
			</li>
    	);
    }
}

export default AccordionItem;